"use strict";

import { NFC, TAG_ISO_14443_3, TAG_ISO_14443_4, KEY_TYPE_A, KEY_TYPE_B } from 'nfc-pcsc';
import { MPC } from 'mpc-js';
const mpc = new MPC();
mpc.connectUnixSocket('/run/mpd/socket');
import pretty from './pretty-logger.js';


// const nfc = new NFC(pretty);
mpc.database.list('Title', [['Artist', 'Loscil'], ['Date', '2006']], ['Album']).then(console.log);

const nfc = new NFC();

// nfc.on('reader', reader => {
//   reader.on('card', async card => {
//     console.log();
//     console.log('card detected', card);

//     try {
//       const data = await reader.read(4, 22);
//       console.log(`data read`, data);
//       const payload = data.toString();
//       console.log(`data converted`, payload);
//     }
//     catch (err) { console.error(`error reading data`, err); }
//   });
// });

nfc.on('reader', reader => {
  reader.on('card', async card => {
    pretty.info(card.uid);

    console.log('Stop play back');
    mpc.playback.stop();
    mpc.currentPlaylist.clear();
    mpc.currentPlaylist.add('').catch(e => {
      pretty.error('Error adding file to playlist', e)
    })
    mpc.playback.play();
  })
})