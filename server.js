import { NFC } from 'nfc-pcsc';
const nfc = new NFC();

import { MPC } from 'mpc-js';
const mpc = new MPC();
mpc.connectUnixSocket('/run/mpd/socket');

import pretty from './pretty-logger.js';
import { getData } from './lib/data.js';

const data = getData();
console.log(data)

nfc.on('reader', reader => {
    reader.on('card', async card => {
      pretty.info(card.uid);
  
      pretty.debug("Stop playback");
      mpc.playback.stop();

      pretty.debug("Clear playlist");
      mpc.currentPlaylist.clear();

      const thisSong = data.find(s => s.uid.toString().toUpperCase() === card.uid.toString().toUpperCase());
      pretty.info("This song found", thisSong);
      mpc.currentPlaylist.add(thisSong.song).catch(e => {
        pretty.error('Error adding file to playlist', e)
      });

      pretty.debug("Play");
      mpc.playback.play();
    })
  })