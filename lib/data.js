import * as yaml from "js-yaml";
import { readFileSync } from "fs";

export function getData() {
    let doc = []
    try {
        doc = yaml.load(readFileSync('./cards.yml', 'utf8'));
    } catch (e) {
        console.log(e);
    }

    return doc;
}